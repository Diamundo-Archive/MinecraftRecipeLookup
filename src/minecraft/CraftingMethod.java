import java.util.ArrayList;

public class CraftingMethod {
	
	private String name;
	private int inputWidth, inputHeight, outputWidth, outputHeight; //for storing the input and output recipe sizes.
	private String description; // Description of a crafting method in the application for the user to read and understand this method
	private ArrayList<String> tags; //for stuff like smeltable
	
	/**
	 * The constructor for the Crafting Method. This is a template for a recipe, which can later be filled in.
	 * This function calls the other constructor, with two extra values for the description and the list of tags, both null.
	 * @param name The name of the crafting method.
	 * @param inputWidth The width of the input grid
	 * @param inputHeight The height of the input grid
	 * @param outputWidth The width of the output grid
	 * @param outputHeight The height of the output grid
	 */
	public CraftingMethod(String name, int inputWidth, int inputHeight, int outputWidth, int outputHeight){
		this(name, inputWidth, inputHeight, outputWidth, outputHeight, null, null);
	}

	/**
	 * The constructor for the Crafting Method. This is a template for a recipe, which can later be filled in.
	 * This function calls the other constructor, with two extra values for the description and the list of tags, both null.
	 * @param name The name of the crafting method.
	 * @param inputWidth The width of the input grid
	 * @param inputHeight The height of the input grid
	 * @param outputWidth The width of the output grid
	 * @param outputHeight The height of the output grid
	 * @param description The description of the crafting method, in case the name does not provide enough information.
	 * @param tags A list of strings, presenting all extra required items.
	 */
	public CraftingMethod(String name, int inputWidth, int inputHeight, int outputWidth, int outputHeight, String description, ArrayList<String> tags){
		this.name = name;
		this.inputWidth = inputWidth;
		this.inputHeight = inputHeight;
		this.outputWidth = outputWidth;
		this.outputHeight = outputHeight;
		this.description = (description == null || description == "" ? "No description yet." : description);
		this.tags = (tags == null || tags.isEmpty() ? null : tags);
	}
	
	public String toString(){
		String returnString = name;
		returnString += "\t" + inputWidth + "\t" + inputHeight + "\t" + outputWidth + "\t" + outputHeight + "\t" + description;
		if(tags != null && !tags.isEmpty()){
			for(String tag : tags){
				returnString += "\t" + tag;
			}
		}
		return returnString;
	}
	
	/**
	 * This method will return a string containing all tags from this method, separated by tabs.
	 * If there are no tags, it will return an empty string. 
	 * @return All tags in a string, separated by a tab.
	 */
	public String getTagsAsString(){
		if(tags != null && !tags.isEmpty()) {
			String returnString = "";
			for(String tag : tags){
				returnString += tag + "\t";
			}
			return returnString;
		} else {
			return "";
		}
	}

	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
	public ArrayList<String> getTags(){
		return tags;
	}
	
	public boolean hasTags(){
		return (tags == null ? false : !tags.isEmpty() );
	}

	public int getInputWidth() {
		return inputWidth;
	}

	public int getInputHeight() {
		return inputHeight;
	}

	public int getOutputWidth() {
		return outputWidth;
	}

	public int getOutputHeight() {
		return outputHeight;
	}
	
	/**
	 * Shorthand for ( getInputWidth() * getInputHeight() ).
	 * @return an integer representing the maximum number of items required in the input field.
	 */
	public int getInputSize() {
		return inputWidth * inputHeight;
	}

	/**
	 * Shorthand for ( getOutputWidth() * getOutputHeight() ).
	 * @return an integer representing the maximum number of items required in the output field.
	 */
	public int getOutputSize() {
		return outputWidth * outputHeight;
	}

}
