import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class CraftingPanel extends JPanel implements MouseListener {
	
	private boolean hasImages;
	private Item item;
	private ItemInfoPanel itemInfoPanel;
	private CraftingInfoPanel craftingInfoPanel;
	
	/**
	 * Constructor for the CraftingPanel. This panel houses the ItemInfoPanel and CraftingInfoPanel.
	 * The two panels are shown underneath one another, the item info panel being the upper panel.
	 * To compensate for the borders created by the BorderFactory, we leave 80 pixels room in between the two panels.
	 * @param width The preferred width of the panel.
	 * @param height The preferred height of the panel.
	 * @see ItemInfoPanel
	 * @see CraftingInfoPanel
	 */
	public CraftingPanel(int width, int height){
		setPreferredSize(new Dimension(width, height));
		itemInfoPanel = new ItemInfoPanel(width, 70);
		craftingInfoPanel = new CraftingInfoPanel(width, (height-150) );
		// leave 80 pixels for the borders
		
		itemInfoPanel.setVisible(true);
		craftingInfoPanel.setVisible(true);
		
		itemInfoPanel.setBorder(BorderFactory.createTitledBorder("Item"));
		craftingInfoPanel.setBorder(BorderFactory.createTitledBorder("Crafting"));
		
		add(itemInfoPanel, BorderLayout.NORTH);
		add(craftingInfoPanel, BorderLayout.SOUTH);
		
		setVisible(true);
		addMouseListener(this);
		hasImages = false;
	}
	
	/**
	 * When the user requests to see all items that contain a certain tag, the ItemInfoPanel is used to display not the name of the item, but the name of the crafting method and the tag.
	 * Furthermore, the border of the CraftingInfoPanel is replaced with an emtpy border, and the border of the ItemInfoPanel is replaced by a titled border containing "Crafting Method".
	 * @param craftingMethod The crafting method to be displayed.
	 * @param tag The tag to be displayed.
	 * @param items The list of items that contain said tag.
	 */
	public void showAllItemsWithTag(CraftingMethod craftingMethod, String tag, ArrayList<Item> items){
		itemInfoPanel.setBorder(BorderFactory.createTitledBorder("Crafting Method"));
		itemInfoPanel.setItemInfoPanel(craftingMethod.getName(), tag);
		craftingInfoPanel.setBorder(BorderFactory.createEmptyBorder());
		craftingInfoPanel.showAllItemsWithTag(tag,items);
		
		revalidate();
		repaint();
	}
	
	/**
	 * When the user wants to see the information of a certain item, this method is executed.
	 * @param item The item to be displayed.
	 * @param recipe The selected recipe to be displayed.
	 * @param craftingMethod Said recipes crafting method.
	 * @param amount The amount the user would like to see the requirements of.
	 */
	public void update(Item item, Recipe recipe, CraftingMethod craftingMethod, int amount){
		this.item = item;
		itemInfoPanel.setBorder(BorderFactory.createTitledBorder("Item"));
		craftingInfoPanel.setBorder(BorderFactory.createTitledBorder("Crafting"));
		itemInfoPanel.setItemInfoPanel(item);
		if(craftingMethod != null && recipe != null){ //item.isCraftable()
			craftingInfoPanel.setCraftingInfoPanel(item, recipe, craftingMethod, amount);
		} else {
			craftingInfoPanel.informNotCraftable(item);
		}
	}
	
	public void enableImages(boolean b) {
		hasImages = true;
	}
	
	public void findImageWithID(String iD){
		if(hasImages){
			if(iD.contains(":")){
				iD = iD.replace(":", "-");
			} else {
				iD += "-0";
			}
			if(new File("itemicons/" + iD + ".png").isFile()){
				// FIXME: ICON (in craftingPanel.itemInfoPanel perhaps?)
			}
		}
	}
	
	public CraftingInfoPanel getCraftingInfoPanel(){
		return craftingInfoPanel;
	}

	public ItemInfoPanel getItemInfoPanel() {
		return itemInfoPanel;
	}
	
	public Item getItem() {
		return item;
	}
	

	@Override
	public void mouseClicked(MouseEvent e) {
		// Empty: do nothing
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// Empty: do nothing
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// Empty: do nothing
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// Grab focus away from the search bar
		grabFocus();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// Empty: do nothing
	}
	
}
