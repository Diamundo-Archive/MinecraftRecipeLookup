import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ItemInfoPanel extends JPanel {
	
	private ArrayList<JLabel> text;
	
	/**
	 * The constructor for the item info panel.
	 * This panel is constructed with a default message saying no item has been selected.
	 * Upon selecting an item, this panel will show the name and ID of the item, as well as all tags.
	 * If a tag is shown with a list of all items containing it, this panel will show the name of the crafting method requiring said tag, as well as the tag itself, and a description of the crafting method.
	 * @param width The preferred width of the panel.
	 * @param height The preferred height of the panel.
	 * @see Item
	 * @see Crafting Method
	 * @see CraftingPanel
	 * @see TagPanel
	 */
	public ItemInfoPanel(int width, int height) {
		setPreferredSize(new Dimension(width, height));
		this.text = new ArrayList<>();
		setText("<html><center>No item has been selected<center></html>");
		setVisible(true);
	}
	
	public void setText(String s){
		clearText();
		text.add(new JLabel(s));
		add(text.get(0));
	}
		
	public void clearText(){
		for(JLabel jlabel : text){
			remove(jlabel);
		}
		text = new ArrayList<>();
	}
	
	/**
	 * When an item has been selected, this method is called to show the item information.
	 * @param item The item to be displayed.
	 */
	public void setItemInfoPanel(Item item){
		String text = "<html><center>" + item.getName() + " (ID " + item.getID() + ")<br>";
		ArrayList<String> tags = item.getTags();
		if(! tags.isEmpty() ){
			if( tags.size() > 1) {
				text += "Tags:  ";
				for(String tag : item.getTags()){
					text += "<i>" + tag + "</i>; ";
				}
			} else {
				text += "Tag: <i>" + tags.get(0) + "</i>";
			}
		}
		text += "</center></html>";
		setText(text);
	}
	
	/**
	 * When a tag has been selected, this method is called to show the crafting method requiring the tag, the tag itself, and the description of the crafting method.
	 * @param craftingMethod The crafting method requiring the tag.
	 * @param tag The tag to be displayed.
	 */
	public void setItemInfoPanel(CraftingMethod craftingMethod, String tag){
		clearText();
		String text = "<html><center>" + craftingMethod.getName() + " requires: <i>" + tag + "</i><br>" + craftingMethod.getDescription() + "</center></html>";
		setText(text);
		System.out.println(method + " " + tag);
	}
	
}
