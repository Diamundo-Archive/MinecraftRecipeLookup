import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class CraftingInfoPanel extends JPanel {

	private RecipePanel	recipePanel;
	private RequirementsPanel requirementsPanel;
	private TagPanel tagPanel;

	/**
	 * The constructor of the Crafting Info Panel. This panel holds the RecipePanel and RequirementsPanel, and the TagPanel.
	 * Not all three are shown together, only the first two when showing an Item, or the latter when showing all Items containing a certain tag.
	 * @param width The preferred width of the panel.
	 * @param height The preferred height of the panel.
	 * @see RecipePanel
	 * @see RequirementsPanel
	 * @see TagPanel
	 */
	public CraftingInfoPanel(int width, int height) {
		setPreferredSize(new Dimension(width, height));
		
		recipePanel = new RecipePanel(28*(width/50), height-28); 
		requirementsPanel = new RequirementsPanel(20*(width/50), height-28);
		// so they have 2/50th space inbetween, otherwise requirementsPanel will get pushed off.
		// and leave 28 pixels for the border underneath
		tagPanel = new TagPanel(width, height-10); 
		
		recipePanel.setBorder( BorderFactory.createTitledBorder("Recipe"));
		requirementsPanel.setBorder( BorderFactory.createTitledBorder("Requirements"));
		
		add(recipePanel, BorderLayout.WEST);
		add(requirementsPanel, BorderLayout.EAST);

		setVisible(true);
		
	}
	
	/**
	 * This function will remove the Recipe and Requirements Panels to make way for the TagPanel, which is then set with the provided tag and the list of all items containing said tag.
	 * @param tag The tag to be shown.
	 * @param items A list of all items containing this tag.
	 */
	public void showAllItemsWithTag(String tag, ArrayList<Item> items){
		if(recipePanel != null) { 
			remove(recipePanel); 
			remove(requirementsPanel);
		}
		
		tagPanel.setBorder(BorderFactory.createTitledBorder(items.size() + " items with tag \"" + tag + "\""));
		add(tagPanel, BorderLayout.NORTH);
		
		tagPanel.update(tag, items);
	}
	
	/**
	 * This function will remove the TagPanel to make way for the Recipe and Requirements Panels, which are then set to show the item, the selected recipe, that recipes crafting method, and the amount of items the user would like to make.
	 * 
	 * @param item The item to be shown.
	 * @param recipe The specific recipe for the item to be shown.
	 * @param craftingMethod The crafting method of said recipe.
	 * @param amount The amount of items the user wants to see the requirements of.
	 */
	public void setCraftingInfoPanel(Item item, Recipe recipe, CraftingMethod craftingMethod, int amount){
		if(tagPanel != null) {
			remove(tagPanel);
			
			recipePanel.setBorder( BorderFactory.createTitledBorder("Recipe"));
			requirementsPanel.setBorder( BorderFactory.createTitledBorder("Requirements"));
			
			add(recipePanel, BorderLayout.WEST);
			add(requirementsPanel, BorderLayout.EAST);
		}
		
		recipePanel.setRecipePanel(item, recipe, craftingMethod);
		requirementsPanel.setRequirementsPanel(item, recipe, amount);
	}
	
	/**
	 * When the user selects an item, but this item does not (yet) have a recipe, a message is displayed to inform this.
	 * @param item The item the user selected, but which does not have any recipes.
	 */
	public void informNotCraftable(Item item){
		recipePanel.informNotCraftable(item);
		requirementsPanel.informNotCraftable();
	}
		
	public RecipePanel getRecipePanel(){
		return recipePanel;
	}
	
	public RequirementsPanel getRequirementsPanel(){
		return requirementsPanel;
	}

	public TagPanel getTagPanel() {
		return tagPanel;
	}
}
