import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class CraftingInfoPanel extends JPanel {

	private RecipePanel	recipePanel;
	private RequirementsPanel requirementsPanel;
	private TagPanel tagPanel;

	public CraftingInfoPanel(int width, int height) {
		setPreferredSize(new Dimension(width, height));
		
		recipePanel = new RecipePanel(28*(width/50), height-28); 
		requirementsPanel = new RequirementsPanel(20*(width/50), height-28);
		// so they have 2/50th space inbetween, otherwise requirementsPanel will get pushed off.
		// and leave 28 pixels for the border underneath
		tagPanel = new TagPanel(width, height-10); 
		
		recipePanel.setBorder( BorderFactory.createTitledBorder("Recipe"));
		requirementsPanel.setBorder( BorderFactory.createTitledBorder("Requirements"));
		
		add(recipePanel, BorderLayout.WEST);
		add(requirementsPanel, BorderLayout.EAST);

		setVisible(true);
		
	}
	
	public void showAllItemsWithTag(String tag, ArrayList<Item> items){
		if(recipePanel != null) { 
			remove(recipePanel); 
			remove(requirementsPanel);
		}
		
		tagPanel.setBorder(BorderFactory.createTitledBorder(items.size() + " items with tag \"" + tag + "\""));
		add(tagPanel, BorderLayout.NORTH);
		
		tagPanel.update(tag, items);
	}
	
	public void setCraftingInfoPanel(Item item, Recipe recipe, CraftingMethod craftingMethod, int amount){
		if(tagPanel != null) {
			remove(tagPanel);
			
			recipePanel.setBorder( BorderFactory.createTitledBorder("Recipe"));
			requirementsPanel.setBorder( BorderFactory.createTitledBorder("Requirements"));
			
			add(recipePanel, BorderLayout.WEST);
			add(requirementsPanel, BorderLayout.EAST);
		}
		
		recipePanel.setRecipePanel(item, recipe, craftingMethod);
		requirementsPanel.setRequirementsPanel(item, recipe, amount);
	}
	
	public void informNotCraftable(Item item){
		recipePanel.informNotCraftable(item);
		requirementsPanel.informNotCraftable();
	}
	
	public RecipePanel getRecipePanel(){
		return recipePanel;
	}
	
	public RequirementsPanel getRequirementsPanel(){
		return requirementsPanel;
	}

	public TagPanel getTagPanel() {
		return tagPanel;
	}
}