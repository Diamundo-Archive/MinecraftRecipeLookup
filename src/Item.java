import java.util.ArrayList;

public class Item implements Comparable<Item> {
	
	private String iD; // In-game item iD
	private String name; // Recognizable item name
	private boolean isObtainable; // Whether an item is obtainable without mods in-game
	private ArrayList<Recipe> recipes; // All recipes with this item as output
	private ArrayList<String> tags; // Tags to make the item more easily searchable AND to specify crafting methods
	
	public Item(String iD, String name, boolean isObtainable, ArrayList<String> tags){
		// Specific item
		this.iD = iD;
		this.name = name;
		this.isObtainable = isObtainable;
		this.tags = tags;
		this.recipes = new ArrayList<>();
	}

	/**
	 * Getters and recipe adder for item fields
	 */
	
	public String getID() {
		return iD;
	}
	
	public String getName() {
		return name;
	}

	public boolean isObtainable() {
		return isObtainable;
	}

	public boolean isCraftable() {
		return !recipes.isEmpty();
	}
	
	public void setID(String iD){
		this.iD = iD;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setObtainable(boolean value){
		this.isObtainable = value;
	}
	
	public void setRecipes(ArrayList<Recipe> recipes){
		this.recipes = recipes;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}
	
	public ArrayList<String> getTags(){
		return tags;
	}
	
	public ArrayList<Recipe> getRecipes(){
		return recipes;
	}
	
	public void addRecipe(Recipe recipe){
		this.recipes.add(recipe);
	}

	public void removeRecipe(Recipe recipe) {
		this.recipes.remove(recipe);
	}
	
	public void addTag(String tag){
		tags.add(tag);
	}
	
	public String getTagsAsString(){
		if(tags == null) return "";
		String returnString = "";
		for(String tag : tags){
			returnString += tag + " ";
		}
		// Return the string minus the last ' '
		return returnString.length() > 0 ? returnString.substring(0, returnString.length()-1) : "";
	}
	
	public int compareInt(int a, int b){
		return a<b ? -1 : (a>b ? 1 : 0); 
	}
	
	@Override
	public int compareTo(Item other){
		int result;
		if(!(this.iD.contains(":") && other.iD.contains(":"))){
			if(this.iD.contains(":")){
				// First has metadata
				result = compareInt(Integer.parseInt(this.iD.split(":")[0]), Integer.parseInt(other.iD));
			} else if(other.iD.contains(":")){
				// Second has metadata
				result = compareInt(Integer.parseInt(this.iD), Integer.parseInt(other.iD.split(":")[0]));
				if(result == 0) result = -1;
			} else {
				// None have metadata
				result = compareInt(Integer.parseInt(this.iD), Integer.parseInt(other.iD));
				if(result == 0) result = 1;
			}
		} else {
			// Both have metadata
			result = compareInt(Integer.parseInt(this.iD.split(":")[0]), Integer.parseInt(other.iD.split(":")[0]));
			if(result == 0){
				result = compareInt(Integer.parseInt(this.iD.split(":")[1]), Integer.parseInt(other.iD.split(":")[1]));
			}
		}
		return result;
	}

	public String toString(){
		String returnString = (iD + '\t' + name + '\t' + isObtainable);
		for(String tag : tags){
			returnString += '\t' + tag;
		}
		return returnString;
	}
	
}
