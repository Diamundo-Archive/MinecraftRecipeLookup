import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ItemInfoPanel extends JPanel {
	
	private ArrayList<JLabel> text;
	
	public ItemInfoPanel(int width, int height) {
		setPreferredSize(new Dimension(width, height));
		this.text = new ArrayList<>();
		setText("<html><center>No item has been selected<center></html>");
		setVisible(true);
	}
	
	public void setText(String s){
		clearText();
		text.add(new JLabel(s));
		add(text.get(0));
	}
		
	public void clearText(){
		for(JLabel jlabel : text){
			remove(jlabel);
		}
		text = new ArrayList<>();
	}
	
	public void setItemInfoPanel(Item item){
		String text = "<html><center>" + item.getName() + " (ID " + item.getID() + ")<br>";
		ArrayList<String> tags = item.getTags();
		if(! tags.isEmpty() ){
			if( tags.size() > 1) {
				text += "Tags:  ";
				for(String tag : item.getTags()){
					text += "<i>" + tag + "</i>; ";
				}
			} else {
				text += "Tag: <i>" + tags.get(0) + "</i>";
			}
		}
		text += "</center></html>";
		setText(text);
	}
	
	public void setItemInfoPanel(String method, String tag){
		clearText();
		String text = "<html><center>" + method + " requires: <i>" + tag + "</i></center></html>";
		setText(text);
		System.out.println(method + " " + tag);
	}
	
}
