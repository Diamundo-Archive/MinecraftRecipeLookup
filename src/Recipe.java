import java.util.ArrayList;


public class Recipe {
	
	private CraftingMethod craftingMethod; // Crafting method (i.e. crafting table, furnace, etc.)
	private ArrayList<Item> output; // List of items resulting from the recipe
	private ArrayList<Item> input; // List of items required for crafting
	
	public Recipe(CraftingMethod method, ArrayList<Item> input, ArrayList<Item> output){
		this.craftingMethod = method;
		this.input = input;
		this.output = output;
	}

	public CraftingMethod getCraftingMethod() {
		return craftingMethod;
	}
	
	public void setCraftingMethod(CraftingMethod craftingMethod) {
		this.craftingMethod = craftingMethod;
	}
	
	public ArrayList<Item> getOutput(){
		return output;
	}
	
	public void setOutput(ArrayList<Item> output) {
		this.output = output;
	}
	
	public ArrayList<Item> getInput() {
		return input;
	}
	
	public void setInput(ArrayList<Item> input) {
		this.input = input;
	}
	
	public void addToInput(Item item){
		input.add(item);
	}
	
	public void removeFromInput(Item item){
		input.remove(item);
	}
	
	public void addToOutput(Item item){
		output.add(item);
	}
	
	public void removeFromOutput(String iD){
		output.remove(iD);
	}
	
	/**
	 * Methods to convert recipes to string format
	 * @return a (partial) string representation of the recipe
	 */
	
	public String getInputIDsAsString(){
		String returnString = "";
		for(Item input : input){
			returnString += input.getID() + " ";
		}
		// Return the string minus the last ' '
		return returnString.length() > 0 ? returnString.substring(0, returnString.length()-1) : "";
	}
	
	public String getOutputIDsAsString(){
		String returnString = "";
		for(Item output : output){
			returnString += output.getID() + " ";
		}
		// Return the string minus the last ' '
		return returnString.length() > 0 ? returnString.substring(0, returnString.length()-1) : "";
	}
	
	public String getInputNamesAsString(){
		String returnString = "";
		for(Item input : input){
			if(!(returnString.contains(input.getName()) || input.getID().equals("0"))){
				returnString += input.getName() + ", ";
			}
		}
		// Return the string minus the last ", "
		return returnString.length() > 1 ? returnString.substring(0, returnString.length()-2) : "";
	}
	
	public String getOutputNamesAsString(){
		String returnString = "";
		for(Item output : output){
			if(!(returnString.contains(output.getName()) || output.getID().equals("0"))){
				returnString += output.getName() + ", ";
			}
		}
		// Return the string minus the last ", "
		return returnString.length() > 1 ? returnString.substring(0, returnString.length()-2) : "";
	}
	
	public String toString(){
        String returnString = craftingMethod.getName() + " <input:>";
        for(Item item : input){
        	returnString += "\t" + item.getID();
        }
        returnString += " <output:>";
        for(Item item : output){
        	returnString += "\t" + item.getID();
        }
        return returnString;
	}
	
}
